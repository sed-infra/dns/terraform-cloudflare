resource "cloudflare_zone" "cedricfarinazzo-dev" {
  account_id = local.cloudflare_account_id
  zone       = "cedricfarinazzo.dev"
}


resource "cloudflare_zone_dnssec" "cedricfarinazzo-dev" {
  zone_id = cloudflare_zone.cedricfarinazzo-dev.id
}

resource "cloudflare_record" "cedricfarinazzo-dev-root" {
  zone_id = cloudflare_zone.cedricfarinazzo-dev.id
  name = "@"
  value = local.ddns_domain_name
  type = "CNAME"
}

resource "cloudflare_record" "cedricfarinazzo-dev-sub" {
  zone_id = cloudflare_zone.cedricfarinazzo-dev.id
  name = "*"
  value = local.ddns_domain_name
  type = "CNAME"
}