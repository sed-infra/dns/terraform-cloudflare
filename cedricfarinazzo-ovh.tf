resource "cloudflare_zone" "cedricfarinazzo-ovh" {
  account_id = local.cloudflare_account_id
  zone       = "cedricfarinazzo.ovh"
}


resource "cloudflare_zone_dnssec" "cedricfarinazzo-ovh" {
  zone_id = cloudflare_zone.cedricfarinazzo-ovh.id
}

resource "cloudflare_record" "cedricfarinazzo-ovh-root" {
  zone_id = cloudflare_zone.cedricfarinazzo-ovh.id
  name = "@"
  value = local.ddns_domain_name
  type = "CNAME"
}

resource "cloudflare_record" "cedricfarinazzo-ovh-sub" {
  zone_id = cloudflare_zone.cedricfarinazzo-ovh.id
  name = "*"
  value = local.ddns_domain_name
  type = "CNAME"
}