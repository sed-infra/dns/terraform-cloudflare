resource "cloudflare_zone" "cedricfarinazzo-fr" {
  account_id = local.cloudflare_account_id
  zone       = "cedricfarinazzo.fr"
}


resource "cloudflare_zone_dnssec" "cedricfarinazzo-fr" {
  zone_id = cloudflare_zone.cedricfarinazzo-fr.id
}

resource "cloudflare_record" "cedricfarinazzo-fr-root" {
  zone_id = cloudflare_zone.cedricfarinazzo-fr.id
  name = "@"
  value = local.ddns_domain_name
  type = "CNAME"
}

resource "cloudflare_record" "cedricfarinazzo-fr-sub" {
  zone_id = cloudflare_zone.cedricfarinazzo-fr.id
  name = "*"
  value = local.ddns_domain_name
  type = "CNAME"
}