provider "cloudflare" {
  #email     = var.cloudflare_email
  api_token = var.cloudflare_api_token
}

data "cloudflare_accounts" "main_account" {
}

locals {
  cloudflare_account_id = data.cloudflare_accounts.main_account.accounts[0].id

  ddns_domain_name = "cedricfarinazzo.ddns.net"
}
