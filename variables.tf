variable "cloudflare_email" {
  type        = string
  description = "Cloudflare email"
  sensitive   = true
}

variable "cloudflare_api_token" {
  type        = string
  description = "Cloudflare api token"
  sensitive   = true
}
